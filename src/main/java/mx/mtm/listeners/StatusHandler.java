package mx.mtm.listeners;

import com.twitter.hbc.twitter4j.handler.StatusStreamHandler;
import com.twitter.hbc.twitter4j.message.DisconnectMessage;
import com.twitter.hbc.twitter4j.message.StallWarningMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;

/**
 * @author Losthaven
 * @since 2015.09.22
 */
public class StatusHandler implements StatusStreamHandler {

    private static final Logger LOG = LoggerFactory.getLogger(StatusHandler.class);

    @Override
    public void onStatus(Status status) {
        LOG.info("[USER:{}; STATUS:{}] Status received", status.getUser().getId(), status.getId());
    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {
        LOG.warn(stallWarning.getMessage());
    }

    @Override
    public void onStallWarningMessage(StallWarningMessage stallWarningMessage) {
        LOG.warn(stallWarningMessage.getMessage());
    }

    @Override
    public void onDisconnectMessage(DisconnectMessage disconnectMessage) {
        LOG.warn(disconnectMessage.getDisconnectReason());
    }

    @Override
    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
        LOG.warn("Current statuses limit: {}", numberOfLimitedStatuses);
    }

    @Override
    public void onUnknownMessageType(String message) {
        LOG.warn(message);
    }

    @Override
    public void onScrubGeo(long userId, long upToStatusId) {
        LOG.info("[USER:{}; STATUS:{}] Location deleted", userId, upToStatusId);
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        LOG.info("[USER:{}; STATUS:{}] Status deleted", statusDeletionNotice.getUserId(), statusDeletionNotice.getStatusId());
    }

    @Override
    public void onException(Exception exception) {
        LOG.warn(exception.getMessage());
    }
}
