package mx.mtm;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import com.twitter.hbc.twitter4j.Twitter4jStatusClient;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import mx.mtm.config.AccountConfiguration;
import mx.mtm.config.Configuration;
import mx.mtm.config.StreamConfiguration;
import mx.mtm.listeners.StatusHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.StatusListener;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Losthaven
 * @since 2015.09.22
 */
public class MainApplication extends Application<Configuration> {

    private static final Logger LOG = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args) throws Exception {
        new MainApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<Configuration> bootstrap) {
        bootstrap.addBundle(new SwaggerBundle<Configuration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(Configuration configuration) {
                return configuration.getSwaggerConfiguration();
            }
        });
    }

    @Override
    public void run(final Configuration configuration, Environment environment) throws Exception {
        LOG.info("Service started. Listening on port {}...", 8080);

        /** Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
        BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1000);

        /** Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        Hosts apiHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint statusesFilterEndpoint = new StatusesFilterEndpoint();

        // Optional: set up some followings and track terms
        StreamConfiguration streamConfiguration = configuration.getStreamConfiguration();
        statusesFilterEndpoint.followings(streamConfiguration.getAccounts());
        statusesFilterEndpoint.trackTerms(streamConfiguration.getKeywords());

        // These secrets should be read from a config file
        AccountConfiguration accountConfiguration = configuration.getAccountConfiguration();
        Authentication authentication = new OAuth1(
            accountConfiguration.getConsumerKey(),
            accountConfiguration.getConsumerSecret(),
            accountConfiguration.getAccessToken(),
            accountConfiguration.getAccessTokenSecret()
        );

        ClientBuilder builder = new ClientBuilder()
            // optional: mainly for the logs
            .name("streamClient")
            .hosts(apiHosts)
            .authentication(authentication)
            .endpoint(statusesFilterEndpoint)
            .processor(new StringDelimitedProcessor(msgQueue))
            // optional: use this if you want to process client events
            .eventMessageQueue(eventQueue);

        Client client = builder.build();

        StatusHandler statusHandler = new StatusHandler();
        List<StatusListener> listeners = Lists.newArrayList(statusHandler);

        ExecutorService executorService = Executors.newFixedThreadPool(1);

        // client is our Client object
        // msgQueue is our BlockingQueue<String> of messages that the handlers will receive from
        // listeners is a List<StatusListener> of the t4j StatusListeners
        // executorService
        Twitter4jStatusClient t4jClient = new Twitter4jStatusClient(
            client,
            msgQueue,
            listeners,
            executorService
        );
        t4jClient.connect();

        // Call this once for every thread you want to spin off for processing the raw messages.
        // This should be called at least once.
        t4jClient.process(); // required to start processing the messages
    }
}
