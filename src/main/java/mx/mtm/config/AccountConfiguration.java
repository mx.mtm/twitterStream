package mx.mtm.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;

/**
 * @author Losthaven
 * @since 2015.09.22
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountConfiguration {

    @Valid
    @JsonProperty("consumer_key")
    private String consumerKey;

    @Valid
    @JsonProperty("consumer_secret")
    private String consumerSecret;

    @Valid
    @JsonProperty("access_token")
    private String accessToken;

    @Valid
    @JsonProperty("access_token_secret")
    private String accessTokenSecret;
}
