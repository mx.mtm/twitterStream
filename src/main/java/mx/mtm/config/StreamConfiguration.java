package mx.mtm.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Losthaven
 * @since 2015.09.23
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StreamConfiguration {

    @Valid
    @JsonProperty("accounts")
    private List<Long> accounts;

    @Valid
    @JsonProperty("keywords")
    private List<String> keywords;

    @Valid
    @JsonProperty("locations")
    private List<String> locations;
}
