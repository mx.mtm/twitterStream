package mx.mtm.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import lombok.Getter;

/**
 * @author Losthaven
 * @since 2015.09.22
 */
@Getter
public class Configuration extends io.dropwizard.Configuration {

    @JsonProperty("account")
    private AccountConfiguration accountConfiguration;

    @JsonProperty("stream")
    private StreamConfiguration streamConfiguration;

    @JsonProperty("swagger")
    private SwaggerBundleConfiguration swaggerConfiguration;

}
